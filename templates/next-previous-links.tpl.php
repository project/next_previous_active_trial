<?php

?>
<div class="nextpre-wrapper">
  <?php if ($previous): ?>
    <div class="previous-blk">
      <?php if ($prev_prefix): ?>
        <div><?php print $prev_prefix; ?></div>
      <?php endif; ?>
      <?php print $previous; ?>
    </div>
  <?php endif; ?>
  <?php if ($next): ?>
    <div class="next-blk">
      <?php if ($next_prefix): ?>
        <div><?php print $next_prefix; ?></div>
      <?php endif; ?>
      <?php print $next; ?>
    </div>
  <?php endif; ?>
</div>